/**
 * JavaDoc
 * Model a ComboLock Opener that take user ability to rotate lock through method called Turnright, TurnLeft, reset and open.
 * @author Bailin WU
 *
 */

package CS49J_Assignment3;


public class ComboLock {
    private static final int MAX_TICK_COUNT = 40;
    private int secrete1;
    private int secrete2;
    private int secrete3;
    private boolean dial1 = false;
    private boolean dial2 = false;
    private boolean dial3 = false;
    private int currentLockNumber = 0 ;
    private int currentDialCount = 0;

    /**
     * Construct a comboloc that take in user's input and store them in to 3 parameter below.
     * @param secrete1
     * @param secrete2
     * @param secrete3
     */

    public ComboLock(int secrete1, int secrete2, int secrete3){
        this.secrete1 = secrete1;
        this.secrete2 = secrete2;
        this.secrete3 = secrete3;
    }

    /**
     * Reset method is use to model condition of lock where it's ready to for rotating combinations
     */

    public void reset() {
        currentLockNumber = 0;
        currentDialCount = 0;
        dial1 = false;
        dial2 = false;
        dial3 = false;
    }

    /**
     * TurnRight and TurnLeft method is use to model movement of turning lock to the right.
     * we take parameter of ticks to calculate what number the lock dial is pointing at
     * we also use CurrentDial count to keep which roation of process is lock opening is
     * Turnright and Turnleft we check the key with currentLockNumber to identify if user put in the right tick
     * @param ticks
     */

    public void turnRight(int ticks){
        currentLockNumber = (currentLockNumber +(MAX_TICK_COUNT - (ticks % MAX_TICK_COUNT))) % MAX_TICK_COUNT;
        if (currentDialCount == 0 && secrete1 == currentLockNumber){
            System.out.println(currentLockNumber + "✓");
            currentDialCount = currentDialCount + 1;
            dial1 = true;
        }

        if (currentDialCount == 2  && secrete3 == currentLockNumber){
            System.out.println(currentLockNumber+ "✓");
            currentDialCount = currentDialCount + 1;
            dial3 = true;
        }

    }

    public void turnLeft(int ticks ){
        currentLockNumber = (currentLockNumber + ticks) % MAX_TICK_COUNT;
        if (currentDialCount == 1 && secrete2 == currentLockNumber){
            System.out.println(currentLockNumber+ "✓");
            currentDialCount = currentDialCount + 1;
            dial2 = true;
        }


    }

    /**
     * The open method is use to identify if all currentLockNumber match with secrete key
     * @return True if the lock is open
     * @return False if the lock is not opened
     */

    public boolean open(){
        if (dial1  && dial2  && dial3 ){
            System.out.println("Successful");
            return true;
        }
        else{
            System.out.println("Unsuccessful");
            return false;
        }
    }



}
