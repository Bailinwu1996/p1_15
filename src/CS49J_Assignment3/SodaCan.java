/**
 * JavaDoc
 * Model a Soda Can with user input height and Diameter
 * @author Bailin WU
 *
 */


package CS49J_Assignment3;

public class SodaCan {
    private double height;
    private double diameter;

    /**
     * Construct a Sodacan Constructure so we can use private height and diameter
     * @param height pass in heigh that user entered
     * @param diameter pass in diameter that user entered
     */

    public SodaCan(double height, double diameter){
        this.height = height;
        this.diameter = diameter;
    }

    /**
     * use height and diameter through calculation to get surface area
     * @return surface area
     */
    public double getSurfaceArea(){
        double radius = diameter/2;
        double surfaceArea = (2 * Math.PI * radius * height) + (2 * Math.PI * radius * radius);
        return surfaceArea;
    }

    /**
     * use height and diameter through calculation to get surface area
     * @return volume
     */
    public double getVolume(){
        double radius = diameter/2;
        double Volume = Math.PI * radius * radius * height;
        return Volume;
    }

}
