/**
 * JavaDoc
 * This assignment models a Master Lock.
 * User have ability to enter three numbers to represetn key combination of the lock
 * User and command program which direction to rotate and number of ticks as movment distance
 * Program will than tell user if the lock is opened succesfully or unsuccesfully
 * @author Bailin Wu
 */



package CS49J_Assignment3;
import java.util.Scanner;

public class P8_1 {
    public static void main(String[] args) {

        //scane user's string input and split the string into different element and store them into a array
        Scanner input = new Scanner(System.in);
        System.out.println("Enter Three Number From 0 - 39 with space");
        String userInput = input.nextLine();
        String[] inputArray = userInput.split(" ", 3);

        //converting string element into integers and store them in specific variable
        int secret1 = Integer.parseInt(inputArray[0]);
        int secret2 = Integer.parseInt(inputArray[1]);
        int secret3 = Integer.parseInt(inputArray[2]);

        //use different methods to rest the lock, rotate the lock and open the lock.
        ComboLock DialMovement = new ComboLock(secret1, secret2, secret3);
        DialMovement.reset();
        DialMovement.turnRight(30);
        DialMovement.turnLeft(5);
        DialMovement.turnRight(25);
        DialMovement.open();
        DialMovement.reset();
        DialMovement.turnRight(30);
        DialMovement.turnLeft(5);
        DialMovement.turnLeft(25);
        DialMovement.open();


    }


}
