/**
 * JavaDoc
 * This assignment models a tool with functionality of calculating surface area and valume of a soda can .
 * User have ability to enter hight and diameter of the soda can to determine volume and surface area of soda can
 * @author Bailin Wu
 */


package CS49J_Assignment3;
import java.util.Scanner;



public class E4_23 {
    public static void main(String[] args) {

        // scan user's input and store string into a array as 2 integers
        Scanner input = new Scanner(System.in);
        System.out.println("Enter Soda Can’s height and diameter, use space to separate elements");
        String userInput = input.nextLine();
        String[] inputArray = userInput.split(" ", 2);

        //changing element with in the array into double and store them into different variable
        double height = Double.parseDouble(inputArray[0]);
        double diameter = Double.parseDouble(inputArray[1]);

        //construct SodaCancreater to send parameter to SodaCan.java for further calculation
        SodaCan sodaCanCreater = new SodaCan(height, diameter);

        //print user volume and surface area
        System.out.printf("Soda Can Volume : %.2f \n", sodaCanCreater.getVolume());
        System.out.printf("Soda Can Area : %.2f \n", sodaCanCreater.getSurfaceArea());

    }

}
