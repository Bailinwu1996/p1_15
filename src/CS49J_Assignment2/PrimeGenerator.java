package CS49J_Assignment2;

public class PrimeGenerator {
    static void nextPrime(int input){
        for(int i = 2; i <= input; i++) {
            if(isPrime(i)){
                System.out.println(i + " ");
            }
        }
    }

    static boolean isPrime(int n) {
        if (n <= 1) {
            return false;
        }
        for(int i = 2; i < n; i++){
            if(n % i == 0){
                return false;
            }
        }
        return true;
    }
}
