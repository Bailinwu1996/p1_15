package CS49J_Assignment2;

public class Card {
    static void getDescription(String input) {
        String[] basicList = {"A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
        String[] basicOutputList = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
        String[] suiteList = {"D", "H", "C", "S"};
        String[] suiteOutputList = {"Diamonds", "Hearts", "Clubs", "Spades"};
        String output1 = "";
        String output2 = "";
        String basic = input.substring(0, 1);
        String suite = input.substring(1);


        for (int i = 0; i < basicList.length; i++) {
            if (basic.equalsIgnoreCase(basicList[i]) || basic.equals(basicList[i])) {
                output1 = basicOutputList[i];
                for (int j = 0; j < suiteList.length; j++) {
                    if (suite.equalsIgnoreCase(suiteList[j]) || suite.equals(suiteList[j])) {
                        output2 = suiteOutputList[j];
                        System.out.println(output1 + " of " + output2);
                    } else if (j == 3 && (output2.equals(""))) {
                        System.out.println(" Unknown ");
                        break;
                    }
                }
            } else if (i == 12 && (output1.equals(""))) {
                System.out.println(" Unknown ");
            }
        }
    }
}

