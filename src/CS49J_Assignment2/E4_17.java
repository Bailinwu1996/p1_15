package CS49J_Assignment2;

import java.util.Scanner;

public class E4_17 {
    public static void main(String[] args) {
        Scanner userinput = new Scanner(System.in);
        System.out.println("Please enter the first time: ");
        int time1 = userinput.nextInt();
        System.out.println("please enter the second time:");
        int time2 = userinput.nextInt();
        // converting hour to min
        int hour1 = time1 / 100;
        int minute1 = time1 % 100;
        int total_min1 = (hour1 * 60) + (minute1);
        int hour2 = time2 / 100;
        int minute2 = time2 % 100;
        int total_min2 = (hour2 * 60) + (minute2);
        int time_diff = total_min2 - total_min1;

        if (time_diff < 0) {
            System.out.println(time_diff);
            time_diff = time_diff + (24 * 60);
            int hour = time_diff / 60;
            int minutes = time_diff % 60;
            System.out.println(hour + " hours " + minutes + " minutes");
        } else {
            int hour = time_diff / 60;
            int minutes = time_diff % 60;
            System.out.println(hour + " hours " + minutes + " minutes");
        }
    }
}
