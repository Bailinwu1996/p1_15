/**
 * The Gradekeeper(E15_2)program implements an application that
 * record the first name, last name, and grade of the student in database
 * user is able to append more student, check the grade of student with their ID
 * user is able to remove student information
 *
 * @author  Bailin Wu
 * @since   2021-04-15
 */

package CS49J_Assignment6;
import java.io.*;
import java.util.Scanner;
import java.util.HashMap;
import java.util.Map;

public class E15_4 {
    /**
     * this is the main function of the code, use to make code rune
     * declared the tree map of student and grade that holds id number and student name,
     * declared the hash map
     * @param args
     */
    public static void main(String[] args) throws IOException {

        Map<String, String> GradeKeeper = new HashMap<String, String>();
        Scanner input = new Scanner(System.in);
        String menuInput = "";



        while(!menuInput.equalsIgnoreCase("q"))
        {
            System.out.println("Select an option: ");
            System.out.println("        (a) to add a student");
            System.out.println("        (r) to remove a student ");
            System.out.println("        (m) to modify a grade");
            System.out.println("        (p) print all grades");
            System.out.println("        (q)to quit");
            System.out.print("Enter here:");
            menuInput = input.next();

            // User is able to use call this method to record student's first and grade
            if(menuInput.equalsIgnoreCase("a"))
            {
                System.out.print("Enter student name: ");
                String stdnName = input.next();
                System.out.print("Enter student grade: ");
                String stdnGrade = input.next();
                GradeKeeper.put(stdnName, stdnGrade);
            }

            //Remove  take user's input of student name to find the student they want to remove from the map
            else if(menuInput.equalsIgnoreCase("r"))
            {
                System.out.println("Enter name to Remove: ");
                String stdnName = input.next();
                GradeKeeper.remove(stdnName);

            }

            //modify  take user's input of student name to find the student they want to modify grade
            else if(menuInput.equalsIgnoreCase("m"))
            {
                System.out.println("grade Change for(Student Name): ");
                String stdnName = input.next();
                if(GradeKeeper.containsKey(stdnName)){
                    GradeKeeper.remove(stdnName);
                    System.out.print("Enter new student grade: ");
                    String stdnGrade = input.next();
                    GradeKeeper.put(stdnName, stdnGrade);
                }

            }

            //User is able to print all information of all student
            else if(menuInput.equalsIgnoreCase("p"))
            {
                for(Map.Entry<String, String>graber : GradeKeeper.entrySet()){
                    System.out.print(graber.getKey() + " ");
                    System.out.println((graber.getValue()));
                }
            }

        }
    }
}
