package CS49J_Assignment6;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Map;
import java.util.LinkedHashMap;

public class E5_15 {
    public static void main(String[] args) throws FileNotFoundException {

        // validating arguments length
        if(args.length != 1) {
            System.err.println("Need a command line argument for the file to read.");
            System.exit(1);
        }

        Map<String, ArrayList<String>> map = new LinkedHashMap<String, ArrayList<String>>();
        Scanner scanner = new Scanner(new File(args[0]));

        // Scan the line, use useDelimiter to identify the word and store the word to display
        while(scanner.hasNextLine()) {
            String line = scanner.nextLine();
            Scanner in = new Scanner(line);
            in.useDelimiter("[^A-Za-z0-9_]+");
            while(in.hasNext())
            {
                String token = in.next();
                ArrayList<String> lines = map.getOrDefault(token, new ArrayList<String>());
                lines.add(line);
                map.put(token, lines);
            }
            in.close();
        }
        scanner.close();

        //counting number of words in sentence 
        int index = 0;
        for(String key : map.keySet())
        {
            System.out.println(index + ": " + key + " occurs in:");
            for(String line : map.get(key))
            {
                System.out.println(line);
            }
            System.out.println();
            ++index;
        }
    }
}
