/**
 * This is the student class E15_2 use to set student's information such as their first, last name, id number and grade.
 */

package CS49J_Assignment6;

public class Student implements Comparable<Student> {
    String stdnFName;
    String stdnLName;
    int stdnID;

    /**
     * Constructor of student class use to set first, last, and id number of student
     * @param firstName
     * @param lastName
     * @param idNum
     */
    public Student(String firstName,String lastName,int idNum) {
        stdnFName = firstName;
        stdnLName = lastName;
        stdnID = idNum;
    }

    /**
     * use to get first name of student for code to operate
     * @return
     */
    public String getFname() {
        return stdnFName;
    }

    /**
     *  use to get Last name of student for code to operate
     * @return
     */
    public String getLname() {
        return stdnLName;
    }

    /**
     * use to get id num of student for code to operate
     * @return
     */
    public int getID() {
        return stdnID;
    }

    /**
     * use to set first name of student for code to operate
     * @return
     */
    public void setFname(String FirstName) {
        stdnFName = FirstName;
    }

    /**
     * use to set last name of student for code to operate
     * @return
     */
    public void setLname(String lastName){
        stdnLName = lastName;
    }

    /**
     * use to set id number of student for code to operate
     * @return
     */
    public void setID(int idNum){
        stdnID = idNum;
    }

    /**
     * output all information  when user use print function
     * @return
     */
    public String toString() {
        return stdnFName + " " + stdnLName+ " " + stdnID;
    }

    @Override

    /**
     * Take advantage of compareTO function to generated method stub
     */
    public int compareTo(Student s) {
        if (this.stdnLName.compareTo(s.stdnLName) == 0) {
            if (this.stdnFName.compareTo(s.stdnFName) == 0) {
                return this.stdnID-s.stdnID;
            }
            return this.stdnFName.compareTo(s.stdnFName);
        }
        return this.stdnLName.compareTo(s.stdnLName);
    }

    /**
     * use to varify student information when adjusting grade
     * @param s
     * @return
     */
    public boolean equals(Student s) {
        if(stdnFName.equals(s.stdnFName) && stdnLName.equals(s.stdnLName) && stdnID==s.stdnID) {
            return true;
        }
        return false;
    }

    /**
     * Hash function use by the string class to comput the hash code
     * @return
     */
    public int hashCode() {
        final int HASH_MULTIPLIER = 29;
        int h = HASH_MULTIPLIER * stdnFName.hashCode() + stdnFName.hashCode();
        h = HASH_MULTIPLIER * h + ((Integer) stdnID).hashCode();
        return h;
    }

}

