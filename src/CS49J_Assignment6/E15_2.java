/**
 * The Gradekeeper(E15_2)program implements an application that
 * record the first name, last name, student id number, and grade of the student in database
 * user is able to append more student, check the grade of student with their ID
 * user is able to remove student information
 *
 * @author  Bailin Wu
 * @since   2021-04-15
 */


package CS49J_Assignment6;
import java.util.Scanner;
import java.util.Map;
import java.util.*;

public class E15_2{
    /**
     * this is the main function of the code, use to make code rune
     * declared the tree map of student and grade that holds id number and student name,
     * declared the hash map
     * @param args
     */

    public static void main(String[] args) {
        Map<Integer,Student> student = new TreeMap<Integer,Student>();
        Map<Student,String> grade = new TreeMap<Student,String>();
        Set<Student> sHash = new HashSet<Student>();


        Scanner input = new Scanner(System.in);
        String menuInput = "";
        while(!menuInput.equalsIgnoreCase("q")){
            System.out.println("Select an option: ");
            System.out.println("        (a) to add a student");
            System.out.println("        (r) to remove a student ");
            System.out.println("        (m) to modify a grade");
            System.out.println("        (p) print all grades");
            System.out.println("        (q)to quit");
            System.out.print("Enter here:");
            menuInput = input.next();

            if(menuInput.equalsIgnoreCase("a")) {
                add(student, grade);
            }
            else if(menuInput.equalsIgnoreCase("r")) {
                remove(student, grade);
            }
            else if(menuInput.equalsIgnoreCase("m")) {
                modify(student, grade);
            }
            else if(menuInput.equalsIgnoreCase("p")) {
                print(grade);
            }
        }
    }

    /**
     *This is the add method
     * User is able to use call this method to record student's first, last name, ID number and grade
     * we check if there is repeat ID, if not, we add a new element to the map
     * @param StudentMap
     * @param GradeMap
     */
    public static void add(Map<Integer,Student> StudentMap, Map<Student,String>GradeMap){
        Scanner input = new Scanner(System.in);

        System.out.print("Enter student First name: ");
        String stdnFName = input.next();
        System.out.print("Enter student Last name: ");
        String stdnLName = input.next();
        System.out.print("Enter student ID number: ");
        int stdnID = input.nextInt();
        System.out.print("Enter student grade: ");
        String stdnGrade = input.next();


        while(StudentMap.containsKey(stdnID))
        {
            System.out.println("Error Student with ID already exists");
            System.out.println("Re-enter ID: ");
            stdnID=input.nextInt();
        }
        Student s = new Student(stdnFName,stdnLName,stdnID);
        StudentMap.put(s.getID(),s);
        GradeMap.put(s, stdnGrade);
    }


    /**
     * Remove method take user's input of student id number to find the student they want to remove from the map
     * @param StudentMap
     * @param GradeMap
     */
    public static void remove(Map<Integer,Student> StudentMap, Map<Student,String>GradeMap){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the ID of Student to remove");
        int stdnID = input.nextInt();
        GradeMap.remove(StudentMap.get(stdnID));
        StudentMap.remove(stdnID);
    }

    /**
     * Modify method let use to use student id to locate element with in the map and modify their grade
     * @param StudentMap
     * @param GradeMap
     */
    public static void modify(Map<Integer,Student>StudentMap, Map<Student,String>GradeMap)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the ID of Student to Modify");
        int stdnID = input.nextInt();
        while (!StudentMap.containsKey(stdnID)){
            System.out.println("Student doesn't exist!");
            System.out.println("Re-enter ID: ");
            stdnID=input.nextInt();
        }
        System.out.println("What is the new grade?");
        String stdnGrade = input.next();
        GradeMap.put(StudentMap.get(stdnID),stdnGrade);
    }

    /**
     * User is able to print all information of all student
     * @param GradeMap
     */
    public static void print(Map<Student,String>GradeMap){
        Set<Student> studentList = GradeMap.keySet();
        for(Student s: studentList) {
            System.out.println(s.toString()+ " " + GradeMap.get(s));
        }
    }
}

