/**
 * This Program is upgraded version of E20_7, instead of using menu button few and more,
 * implemented a slide bar that allow user to scrol from left to right to increase number of
 * rectangle by double
 *
 *  * @author  Bailin Wu
 *  * @since   2021-05-05
 *
 */
package CS49J_Assignment9;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class E20_1 extends JFrame {

    private int rectangle_count=1;
    private Random random = new Random();

    public static void main(String[] args) {
        //use invokeLater function in swingUtilities to create the frame ad the size
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run() {
                E20_1 r = new E20_1();
                r.setTitle("E20_1: Random Rectangle Generator/ scrool edition");
                r.setSize(new Dimension(500, 500));
                r.setVisible(true);
            }
        });
    }

    public E20_1()
    {
        super();
        setLayout(new BorderLayout());


        @SuppressWarnings("unused")
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 500, 1);

        //implementing the changelistener to repaint by action of scrolling
        slider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent arg0) {
                rectangle_count = slider.getValue()*2;
                E20_1.this.repaint();
            }
        });


//On clicking SHRINK(FEWER),
//half the number of rectangles in the RectangleArea
//with different co-ordinates.


        JPanel rectangleArea = new JPanel()
        {
            public void paintComponent(Graphics g) //in-built function.
            {
                super.paintComponent(g);
                g.setColor(Color.BLUE);
                //get the random size and cordinate of the rectangle
                for(int i=0;i<rectangle_count;i++){
                    int a= (int)Math.floor(random.nextDouble()*getWidth());
                    int b=(int)Math.floor(random.nextDouble()*getHeight());
                    int c = (int)Math.floor(random.nextDouble()*(getWidth()-a));
                    int d = (int)Math.floor(random.nextDouble()*(getHeight()-b));
                    g.drawRect(a,b,c,d);
                }
            }

        };

    //    Add the buttons and the panel to the BorderLayout
    //    so that they are visible for demo.
        add(slider,BorderLayout.NORTH);
        add(rectangleArea,BorderLayout.CENTER);
    }

}
