/**
 * This create a window where user can create a rectangle with ramdom size at random location with in
 * the window, the rectangle size is randomw, the number
 * of rectangle is increase or decrease by the numberby 2 times
 *
 *  * @author  Bailin Wu
 *  * @since   2021-05-05
 *
 */


package CS49J_Assignment9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;

public class E20_7
{

    private final int WIDTH = 600;
    private final int HEIGHT = 800;
    private final int BOX_MAX_W = 80;
    private final int BOX_MAX_H = 100;
    private JFrame frame;
    private ArrayList<Recs> recList;


    /**
     * constructor that set the frame and components that will go into the frame such as
     * menu and the buttons
     */
    public E20_7()
    {

        frame = new JFrame("E20_7: Random Rectangle Generator");

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Rectangles menu");

        JMenuItem fewButton = new JMenuItem("Fewer");
        JMenuItem moreButton = new JMenuItem("More");
        fewButton.addActionListener(new buttonClick(false));
        moreButton.addActionListener(new buttonClick(true));
        menu.add(fewButton);
        menu.add(moreButton);

        menuBar.add(menu);
        frame.setJMenuBar(menuBar);
        frame.setTitle("E20.7");
        frame.setSize(WIDTH, HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        recList = new ArrayList<Recs>();
    }

    // inner class of action listener for buttons

    /**
     * Actions listener that detect the event of user clicking the fewer or more
     * using overide to change current state
     */
    class buttonClick implements ActionListener
    {
        private boolean moreButton;
        public buttonClick (boolean more)
        {
            this.moreButton = more;
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            drawRecs(moreButton);

        }

    }

    @SuppressWarnings("serial")
    class Recs extends JComponent
    {
        Rectangle box;
        Color color;

        public Recs(int x, int y, int w, int h, Color color)
        {
            this.box = new Rectangle(x, y, w, h);
            this.color = color;
        }

        public void paint(Graphics g)
        {
            Graphics2D g2s = (Graphics2D) g;
            g2s.setColor(color);
            g2s.draw(box);
        }
    }

    // method to draw rectangles, putting their size with the math.random function
    public void drawRecs(boolean more)
    {
        int numberOfRectangle = recList.size();
        if (more)
        {
            if (numberOfRectangle == 0)
            {
                numberOfRectangle++;
            }
            else
            {
                numberOfRectangle *= 2;
            }

            for (int i = recList.size(); i < numberOfRectangle; i++)
            {
                int w = (int) (Math.random() * (BOX_MAX_W));
                int h = (int) (Math.random() * (BOX_MAX_H));
                int x = (int) (Math.random() * (WIDTH - w));
                int y = (int) (Math.random() * (HEIGHT - h));

                Recs box = new Recs(x, y, w, h, Color.BLACK);
                recList.add(box);
                frame.add(box);
                frame.setVisible(true);
            }

        }
        else
        {
            numberOfRectangle /= 2;
            for (int i = recList.size() - 1; i >= numberOfRectangle; i--)
            {
                Recs box = recList.get(recList.size() - 1);
                frame.remove(box);
                recList.remove(recList.size() - 1);
                frame.repaint();

            }
        }
    }

    public static void main(String[] args)
    {
        E20_7 frame = new E20_7();
    }

}