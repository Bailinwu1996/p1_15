/**
 * this application is a combo box containing three items labeled “Red”,
 * “Green”, and “Blue” that change the background color of a panel in the center
 * of the frame to red, green, or blue.
 *
 *  * @author  Bailin Wu
 *  * @since   2021-05-05
 *
 */

package CS49J_Assignment9;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class E20_5 {

    public static void main(String args[]) {
        JFrame frame = new JFrame("E20_5: Color Changing Combo Box");


        //Create a Panel with location of panel inside frame(x-coordinate, y-coordinate, width, height and setting default background of panel to grey
        JPanel panel=new JPanel();
        panel.setBounds(75,75,140,100);
        panel.setBackground(Color.gray);

        //Create a combo box with different color selection
        JComboBox box = new JComboBox();
        box.addItem("select a color");
        box.addItem("Red");
        box.addItem("Green");
        box.addItem("Blue");

        //set location of combo box inside frame (x-coordinate, y-coordinate, width, height)
        box.setBounds(75,10,140,30);



        //add combo box and panel to frame
        frame .add(box);
        frame .add(panel);

        //Create a frame to hold components
        frame .setSize(300,300);
        frame .setLayout(null);
        frame .setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);



        //Add action listener event to combo box
        box.addActionListener(new ActionListener() {

            //This method will be called on changing item of combo box
            @Override
            public void actionPerformed(ActionEvent e) {
                //Get selected combo box item element
                String color = box.getSelectedItem().toString();
                //Add switch case on color
                switch (color) {
                    case "Green":
                        //change background color of panel to Green
                        panel.setBackground(Color.GREEN);
                        break;
                    case "Red":
                        //change background color of panel to Red
                        panel.setBackground(Color.RED);
                        break;
                    case "Blue":
                        //change background color of panel to Blue
                        panel.setBackground(Color.BLUE);
                        break;
                }
            }
        });
    }
}
