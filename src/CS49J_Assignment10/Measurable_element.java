package CS49J_Assignment10;

/**
 * this class  implements measurable and converts different data type into a measurable data type
 * * @param <T> template type
 */

public class Measurable_element<T> implements Measurable<T> {
    private T obj;

    public Measurable_element(T obj)
    {
        this.obj = obj;
    }


    @Override
    public double getMeasure() {
        if(obj instanceof Integer)
            return ((Integer)((Object)obj));
        else
            return (((double)((Object)obj)));
    }
}


