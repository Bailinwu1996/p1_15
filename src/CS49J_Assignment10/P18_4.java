/**
 * allow element with different type to be stored in same arary
 * and compare them using largeset function to identify the largest
 *
 *  * @author  Bailin Wu
 *  * @since   2021-05-17
 *
 */

package CS49J_Assignment10;

import java.util.ArrayList;

public class P18_4{

    public static  <T extends Measurable<T>>  T largest(ArrayList<T> list){
        //Initialize the answer
        T ans = list.get(0);
        //Loop through all elements
        for(T obj : list){
            //Check the max, and update the max
            if(obj.getMeasure()>ans.getMeasure()){
                ans = obj;
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        Measurable_element[] elements = new Measurable_element[5];
        elements[0] = new Measurable_element<Integer>(3);
        elements[1] = new Measurable_element<Integer>(6);
        elements[2] = new Measurable_element<Integer>(9);

        PairUtil<Double, Double> pair = PairUtil.minmax(elements);

        ArrayList<Measurable_element> list = new ArrayList();
        list.add(elements[0]);
        list.add(elements[1]);
        list.add(elements[2]);
        System.out.println("Largest : "+largest(list).getMeasure());
    }
}