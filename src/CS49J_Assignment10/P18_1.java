/**
 * allow element with different type to be stored in same arary
 * and compare them using pairutil function
 *
 *  * @author  Bailin Wu
 *  * @since   2021-05-17
 *
 */

package CS49J_Assignment10;

public class P18_1{
    public static void main(String[] args) {
        Measurable_element[] Array_elements = new Measurable_element[5];
        Array_elements[0] = new Measurable_element<Integer>(3);
        Array_elements[1] = new Measurable_element<Double>(5.5);
        Array_elements[2] = new Measurable_element<Double>(99.0);
        Array_elements[3] = new Measurable_element<Integer>(1);

        PairUtil<Double, Double> pair = PairUtil.minmax(Array_elements);
        System.out.println("The min-max pair is: " + pair);
    }
}
