package CS49J_Assignment10;
/**
 * measurable interface
 */

public interface Measurable<T> {
        double getMeasure();
}

