package CS49J_Assignment10;

/**
 * Counts how many words in a file.
 *   @author  Bailin Wu
 *   @since   2021-05-05
 */

public class WordCount {
    public static void main(String[] args) {

        WordCountRunnable wcr = null;
        Thread t = null;
        // A new thread is created for EACH argument (file) provided
        for (int i = 0; i < args.length; i++) {
            t = new Thread(new WordCountRunnable(args[i]));
            t.start();
        }

        //Hold untill all thread finish running
        while (t.isAlive()){
            System.out.println("Some Still thread running");
        }
        //Get the total word count
        System.out.println("\nAll thread "+ WordCountRunnable.THREAD_COUNT +" Have finished running");
        wcr.combinedWordCount();

    }
}
