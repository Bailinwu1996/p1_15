package CS49J_Assignment10;

/**
 * Counts how many words in a file.
 *   @author  Bailin Wu
 *   @since   2021-05-17
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Counts how many words in a file.
 */
class WordCountRunnable implements Runnable {
    private String filename;

    private ReentrantLock lock = new ReentrantLock();

    //Threads count
    private static int count=0;
    public static int THREAD_COUNT;
    private static int wordCounter=0;


    /**
     * Constructs a WordCountRunnable object with a file name.
     *
     * @param aFilename the file name that needs to count words
     */
    public WordCountRunnable(String aFilename) {
        filename = aFilename;
    }

    public void run() {

        threadCount(true);

        // initialize a counter
        int counter = 0;
        // create Scanner - can use, ...
        Scanner sc = null;
        try {
            sc = new Scanner(new FileInputStream(filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // don't forget try-catch
        // while loop
        while (sc.hasNext()) {
            sc.next();
            counter++;
            wordCounter = counter;
        }
        // count individual words
        // what are you going to catch?
        // print statement of the format: filename: wordCount
        // System.out.println(filename + " : " + wordCounter);

        threadCount(false);


    }

    void threadCount(boolean isIncreaseThread){
        // Lock and unlock thread count
        //as required;
        lock.lock();
        try {
            if (isIncreaseThread) {
                count = count+1;//Increment
                THREAD_COUNT = THREAD_COUNT +1;
            }
            else {
                count = count - 1;//Decrement
            }
        } finally {
            //unlock the thread after finish count manipulation
            lock.unlock();
        }
    }

    void combinedWordCount(){
        // count individual words
        // what are you going to catch?
        // print statement of the format: filename: wordCount
        System.out.println(filename + ": " + wordCounter+" words");
    }
}