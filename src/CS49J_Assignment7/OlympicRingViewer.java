/**
 *In Olympic Ring view we created the the frame where the graphic display to user.
 * it have the size of 400 in width and 2500 in height (pixels)
 *
 * @author  Bailin Wu
 * @since   2021-04-15
 */

package CS49J_Assignment7;

import javax.swing.*;

public class OlympicRingViewer
{
    public static void main(String[] args)
    {
        JFrame frame = new JFrame();
        frame.setSize(400,250);
        frame.setTitle("Concentric Circles");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JComponent component =  new OlympicRingComponent();
        frame.add(component);

        frame.setVisible(true);

    }
}

