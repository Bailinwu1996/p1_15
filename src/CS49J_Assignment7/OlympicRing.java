/**
 *In Olympic Ring is where we actually create the rings
 *
 * @author  Bailin Wu
 * @since   2021-04-15
 */

package CS49J_Assignment7;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

public class OlympicRing {
    private int x;
    private int y;
//    private int BOX_X;
//    private int BOX_Y;

    /**
     * pass the x and y set on olymic ring component for later use
     * @param centerX
     * @param centerY
     */
    public OlympicRing(int centerX, int centerY){
        x = centerX;
        y = centerY;
//        BOX_X = 0;
//        BOX_Y = 0;
    }

    public void draw(Graphics2D g1){

        // blue ring with thickness of 10
        g1.setColor(Color.BLUE);
        g1.setStroke(new BasicStroke(10));
        g1.drawOval(x,y,100,100);

        // Black ring with thickness of 10
        g1.setColor(Color.black);
        g1.setStroke(new BasicStroke(10));
        g1.drawOval(x+110, y, 100, 100);

        // red ring with thickness of 10
        g1.setColor(Color.red);
        g1.setStroke(new BasicStroke(10));
        g1.drawOval(x+220, y, 100, 100);

        // yellow ring with thickness of 10
        g1.setColor(Color.yellow);
        g1.setStroke(new BasicStroke(10));
        g1.drawOval(x+50, y+50, 100, 100);

        // green ring with thickness of 10
        g1.setColor(Color.green);
        g1.setStroke(new BasicStroke(10));
        g1.drawOval(x+160, y+50, 100, 100);


    }
}