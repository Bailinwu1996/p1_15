/**
 *In Olympic Ring Component we created the onlympic ring class that contains property of is location
 *
 * @author  Bailin Wu
 * @since   2021-04-15
 */


package CS49J_Assignment7;

import java.awt.*;
import javax.swing.JComponent;

public class OlympicRingComponent extends JComponent
{
    private static final long serialVersionUID = 1L;

    public void paintComponent(Graphics g) {
        // Recover Graphics2D
        Graphics2D g2 = (Graphics2D) g;

        OlympicRing box1 = new OlympicRing(50,50);
        box1.draw(g2);

    }
}