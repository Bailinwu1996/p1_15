/**
 *The Ellipse Program create a pop up frame that draws a circle where the side of circle touch the edge of the frame.
 *The the circle is resizable with size change of the frame
 *
 * @author  Bailin Wu
 * @since   2021-04-15
 */

package CS49J_Assignment7;
import javax.swing.JFrame;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import javax.swing.JComponent;

public class Ellipse {
    public static void main(String[] args) {

        //creating the a frame that is size of 150 in width and 200 in hight(pixel)
        JFrame frame = new JFrame();
        frame.setSize(150, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        EllipseComponent component = new EllipseComponent();
        frame.add(component);
        frame.setVisible(true);
    }
}


class EllipseComponent extends JComponent{
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Graphics2D g1 = (Graphics2D) g;

        //create a ellopse object with ellipse2d class
        //getwidth and height function give us the ability to change size of circle when changing the frame
        Ellipse2D.Double ellipse = new Ellipse2D.Double(0,0,getWidth(),getHeight());

        //drawing both circle with their color
        g2.setColor(Color.CYAN);
        g2.fill(ellipse);
        g1.setColor(Color.BLACK);
        g1.setStroke(new BasicStroke(10));
        g1.draw(ellipse);
    }
}
