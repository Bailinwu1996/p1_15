/**
 * This is a saving account calculator
 * It contains three in place where user enter they info and program will use those
 * info to calculate the saving and output how much user will earn every year
 *
 * @author  Bailin Wu
 * @since   2021-04-29
 */

package CS49J_Assignment8;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JFrame;



public class E20_3 extends JFrame{

    public static void main(String[] args)
    {
        JFrame f = new JFrame("Interest calculator");

        final JTextField savingTB, interestTB, yrNumTB;
        final JPanel savingP, interestP, yrNumP, buttonP, resultBoxP;

        savingP = new JPanel();
        savingTB = new JTextField(10);
        savingP.setBounds(150, 50, 300, 40);
        JLabel savingLab = new JLabel("Initial savings amount:");
        savingP.add(savingLab);
        savingP.add(savingTB);

        interestP = new JPanel();
        interestTB=new JTextField(10);
        interestP.setBounds(150, 100, 300, 40);
        JLabel interestLab = new JLabel("Annual Interest Rate:");
        interestP.add(interestLab);
        interestP.add(interestTB);

        yrNumP = new JPanel();
        yrNumTB = new JTextField(10);
        yrNumP.setBounds(150, 150, 300, 40);
        JLabel yrNumLab = new JLabel("Number of years:");
        yrNumP.add(yrNumLab);
        yrNumP.add(yrNumTB);

        buttonP = new JPanel();
        JButton calcButton =new JButton("Calculate");
        calcButton.setSize(200,30);
        buttonP.add(calcButton);
        buttonP.setBounds(200,200,200,30);

        resultBoxP = new JPanel();
        JTextArea resultBox = new JTextArea(10,10); //JTextarea object
        JScrollPane scrollableResultBox = new JScrollPane(resultBox);
        scrollableResultBox.setBounds(200,250,200,200);
        resultBox.setEditable(false);
        resultBoxP.add(scrollableResultBox);
        scrollableResultBox.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollableResultBox.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

//      adding all the components to JFrame object
        f.add(savingP);
        f.add(interestP);
        f.add(yrNumP);
        f.add(buttonP);
        f.add(scrollableResultBox);

        f.setSize(600,600); //setting frame size
        f.setLayout(null);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);


        calcButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //allow user to make mistake by catching their mistakes and re-enter their input
                try{
                    resultBox.setText("");
                    //turn the string entered by the user in to a double using parse function
                    //save it into balance variable as double
                    double savingsAmount = Double.parseDouble(savingTB.getText().trim());
                    double interestRate = Double.parseDouble(interestTB.getText().trim());
                    int noy=Integer.parseInt(yrNumTB.getText().trim());
                    double balance=savingsAmount;

                    //calculating saving amount for each year
                    for(int i=1;i<=noy;i++)
                    {
                        balance=balance+((interestRate*balance)/100);
                        resultBox.append("Balance in year "+i+" = "+balance+"\n");
                    }
                }
                catch(Exception excep)
                {
                    resultBox.setText("Enter Valid Values!");
                    savingTB.setText("");
                    interestTB.setText("");
                    yrNumTB.setText("");
                }
            }
        });

    }
}
