package CS49J_Assignment8;
/**
 *This program draw a car and output a animation of a car moving from top left to
 * bottom right
 *
 * @author  Bailin Wu
 * @since   2021-04-29
 */

import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;


public class E10_22 {
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 400;

    //setting the frame and where animation is shown
    public static void main(String[] args){
        JFrame frame = new JFrame();
        frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
        frame.setTitle("an animated Car");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        CarComponent component = new CarComponent();
        frame.add(component);

        frame.setVisible(true);


        //use timerlistener class to implement actionlistener that move the car by 1
        //pixel down and 1 pixel right
        class TimerListener implements ActionListener {
            public void actionPerformed(ActionEvent event){
                component.moveCarBy(1,1);
                component.repaint();
            }
        }

        // timer determine how fast the car moves
        ActionListener listener = new TimerListener();
        final int DELAY = 100;
        Timer t = new Timer(DELAY, listener);
        t.start();


    }
}
