package CS49J_Assignment8;

/**
 *Car component is where programer create the shape of the car
 *
 * @author  Bailin Wu
 * @since   2021-04-129
 */

import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JComponent;

/**
 * CarComponenet class is a class where porgramer make paintfunction and moving car function
 * that display on the frame
 */
public class CarComponent extends JComponent {
    public int BOX_X = 0;
    public int BOX_Y = 0;

    Car movingCar = new Car(BOX_X, BOX_Y);


    /**
     * ceate a function that can draw the componenets
     * @param g
     */
    public void paintComponent(Graphics g){
        Graphics2D g2 = (Graphics2D) g;
        movingCar.draw(g2);
    }

    /**
     * create a function that takes dx and dy to determine how much to move
     * animates the moving car when its called
     * @param dx
     * @param dy
     */
    public void moveCarBy(int dx, int dy) {
        movingCar.translate(dx, dy);
        repaint();
    }
}


