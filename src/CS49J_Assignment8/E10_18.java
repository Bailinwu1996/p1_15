 /**
 * This program output a window that shows use two button. Every time
  * the use enter the button, the program is able to determine which button
  * the user pressed. The program will then count and output the number of times
  * user press on the button
 */

 package CS49J_Assignment8;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class E10_18 {
    private static final int FRAME_WIDTH = 100;
    private static final int FRAME_HEIGHT = 60;

    /**
     * where the frame and button is created.
     * the buttons clicks by users is being recorded as events use later
     * @param args
     */
    public static void main(String[] args){
        JFrame frame = new JFrame();
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JButton button1 = new JButton("click me!");
        JButton button2 = new JButton("click me!");
        panel.add(button1);
        panel.add(button2);
        frame.add(panel);

        ActionListener listener = new ClickListener(button1, button2);
        button1.addActionListener(listener);
        button2.addActionListener(listener);

        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    /**
     * clickerlistener class initate the buttons that will display on frame
     * and counter that count number of times button is being pressed
     */
    public static class ClickListener implements ActionListener {
        private JButton button1;
        private JButton button2;
        private int counter1;
        private int counter2;

        /**
         * construct a clickListener
         * @param button1
         * @param button2
         */
        public ClickListener(JButton button1, JButton button2){
            this.button1 = button1;
            this.button2 = button2;
            this.counter1 = 0;
            this.counter2 = 0;
        }

        /**
         * the event is recording the action of program user clicking the button
         * it allow program to override the number of counts
         * @param event
         */
        @Override
        public void actionPerformed(ActionEvent event){
            if(event.getSource() == button1){
                counter1 ++;
                System.out.println("I was clicked " + counter1 + " times!");
            }
            else if(event.getSource() == button2){
                counter2 ++;
                System.out.println("I was clicked " + counter2 + " times!");
            }
        }
    }

}



