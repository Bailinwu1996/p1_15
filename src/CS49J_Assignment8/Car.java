/**
 *Car is where the line of car is being painted and the shape combinded to make
 * a car
 *
 * @author  Bailin Wu
 * @since   2021-04-15
 */


package CS49J_Assignment8;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;


public class Car {
    private int BOX_X;
    private int BOX_Y;

    /**
     * Take use x and y as the starting cordinate of the car
     * @param x
     * @param y
     */
    public Car(int x, int y) {
        BOX_X = x;
        BOX_Y = y;
    }

    /**
     * use g1 to draw the shapes and lines that make up to a car
     * @param g1
     */
    public void draw(Graphics2D g1) {
        Rectangle body = new Rectangle(BOX_X, BOX_Y+10, 60, 10);
        Ellipse2D.Double frontTire = new Ellipse2D.Double(BOX_X + 10, BOX_Y+20, 10,10);
        Ellipse2D.Double rearTire = new Ellipse2D.Double(BOX_X + 40, BOX_Y+20, 10,10);

        Point2D.Double r1 = new Point2D.Double(BOX_X + 10, BOX_Y+10);
        Point2D.Double r2 = new Point2D.Double(BOX_X + 20, BOX_Y);
        Point2D.Double r3 = new Point2D.Double(BOX_X + 40, BOX_Y);
        Point2D.Double r4 = new Point2D.Double(BOX_X + 50, BOX_Y+10);

        Line2D.Double roofTop = new Line2D.Double(r2, r3);
        Line2D.Double frontWind = new Line2D.Double(r1, r2);
        Line2D.Double backWind = new Line2D.Double(r3, r4);

        g1.draw(body);
        g1.draw(frontTire);
        g1.draw(rearTire);
        g1.draw(roofTop);
        g1.draw(frontWind);
        g1.draw(backWind);
    }

    /**
     * method for car component to call so they can move the car
     * @param dx
     * @param dy
     */
    public void translate(int dx, int dy) {
        BOX_X += dx;
        BOX_Y += dy;

    }
}


