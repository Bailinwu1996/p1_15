package CS49J_Assignment4.Problem9_3;

import java.util.Scanner;

/**
 * JavaDoc, this is simplified version of P9_4 more description of program is in P9_4
 */

public class P9_3 {
    public static void main(String[] args){
        Appointment[] appointments = new Appointment[3];
        appointments[0] = new Daily(2016, 1, 1, "morning coffee");
        appointments[1] = new Monthly(2016, 1, 31, "check bank");
        appointments[2] = new Onetime(1996, 8, 4, "HW 4");


        System.out.println("Enter a day with format of(year, month, day) to list " + "appointments: ");
        Scanner userInput = new Scanner(System.in);
        int year = userInput.nextInt();
        int month = userInput.nextInt();
        int day = userInput.nextInt();

        for( int i = 0; i < appointments.length; i++){
            if(appointments[i].occursOn(year,month,day)){
                System.out.println(appointments[i]);
            }
        }
    }
}
