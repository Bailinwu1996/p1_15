package CS49J_Assignment4.Problem9_3;

/**
 * JavaDoc, this is simplified version of P9_4 more description of program is in Monthly_P9_4
 */

public class Monthly extends Appointment {
    public Monthly(int year, int month, int day, String description){
        super(year, month, day, description);
    }

    public boolean occursOn(int year, int month, int day){
        if (year < getYear()){
            return false;
        }
        if (month < getMonth() && year == getYear()){
            return false;
        }
        return day == getDay();
    }

    public String toString(){
        return "Monthly Appointment :" + super.getDescription();
    }

}
