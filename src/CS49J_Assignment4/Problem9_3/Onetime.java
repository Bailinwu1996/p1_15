package CS49J_Assignment4.Problem9_3;

/**
 * JavaDoc, this is simplified version of P9_4 more description of program is in OneTime
 */


public class Onetime extends Appointment {
    public Onetime(int year, int month, int day, String description){
        super(year, month, day, description);
    }

    public String toString(){
        return "One Time Appointment :" + super.getDescription();
    }

}
