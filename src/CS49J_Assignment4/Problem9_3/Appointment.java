package CS49J_Assignment4.Problem9_3;


/**
 * JavaDoc, this is simplified version of P9_4 more description of program is in appointment_P9_4
 */

public class Appointment {
    private String description;
    private int year;
    private int month;
    private int day;

    public Appointment(int year, int month, int day, String description){
        this.year = year;
        this.month = month;
        this.day = day;
        this.description = description;
    }

    public int getYear(){
        return year;
    }

    public int getMonth(){
        return month;
    }

    public int getDay(){
        return day;
    }

    public String getDescription(){
        return description;
    }

    public boolean occursOn(int year, int month, int day){
        return (year == this.year) && (month == this.month) && (day == this.day);
    }

    public String toString(){
        return description;
    }
}
