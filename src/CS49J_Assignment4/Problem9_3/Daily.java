package CS49J_Assignment4.Problem9_3;

/**
 * JavaDoc, this is simplified version of P9_4 more description of program is in Daily_P9_4
 */

public class Daily extends Appointment {
    public Daily(int year, int month, int day, String description) {
        super(year, month, day, description);
    }

    public boolean occursOn(int year, int month, int day){
        return (year == super.getYear()) && (month == super.getMonth() && (day == super.getDay()));
    }

    public String toString(){
        return "One Time Appointment :" + super.getDescription();
    }


}

