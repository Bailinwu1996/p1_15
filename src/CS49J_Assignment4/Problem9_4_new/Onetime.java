package CS49J_Assignment4.Problem9_4_new;

import java.time.LocalDate;


/**
 * extends appointment class  for one time appointment
 */
public class Onetime extends Appointment {

    /**
     * constructs Onetime object
     * @param strDate yyyy-mm-dd
     * @param strDescription description on appointment
     */
    public Onetime(String strDate, String strDescription)
    {
        super(strDate, strDescription);
    }

    /**
     * check to see if occursOn is true/false
     * @param nYear  year
     * @param nMonth month
     * @param nDay day
     * @return true or false.
     */
    public boolean occursOn(int nYear, int nMonth, int nDay)
    {
        return getDate().equals(LocalDate.of(nYear, nMonth,nDay));
    }


}
