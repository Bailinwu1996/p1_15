package CS49J_Assignment4.Problem9_4_new;

import java.util.ArrayList;
import java.util.Scanner;


public class P9_4_new {
    public static void main(String[] args) {
        ArrayList<Appointment> arrAppointments = new ArrayList<Appointment>(); // Create an ArrayList object
        Scanner input = new Scanner(System.in);
        String menuInput = "";

        while(!menuInput.equalsIgnoreCase("q"))
        {
            System.out.println("Select an option: A for add an appointment, C for checking, Q for quit:");
            menuInput = input.next();
            input.nextLine();
            if(menuInput.equalsIgnoreCase("a"))
            {
                String apntType = "";
                while(!apntType.equalsIgnoreCase("b")){
                    System.out.println("Enter the type (O - Onetime, D - Daily, M - Monthly or B - Back to Main menu");
                    apntType = input.next();
                    input.nextLine();

                    if(apntType.equalsIgnoreCase("o")){
                        try{
                            System.out.println("Enter the date (yyyy-mm-dd):");
                            String strDate = input.next();
                            System.out.println("Enter the description: ");
                            String strDescription = input.next();
                            arrAppointments.add(new Onetime(strDate,strDescription));
                            break;
                        }
                        catch(Exception ArrayIndexOutofBoundsException){
                            System.out.println("Invalid Format of date, Please follow format : yyyy-mm-dd");
                        }
                    }
                    else if(apntType.equalsIgnoreCase("d"))
                    {
                        try{
                            System.out.println("Enter the date (yyyy-mm-dd):");
                            String strDate = input.next();
                            System.out.println("Enter the description: ");
                            String strDescription = input.next();
                            arrAppointments.add(new Onetime(strDate,strDescription));
                            break;
                        }
                        catch(Exception ArrayIndexOutofBoundsException){
                            System.out.println("Invalid Format of date, Please follow format : yyyy-mm-dd");
                        }
                    }
                    else if(apntType.equalsIgnoreCase("m"))
                    {
                        try{
                            System.out.println("Enter the date (yyyy-mm-dd):");
                            String strDate = input.next();
                            System.out.println("Enter the description: ");
                            String strDescription = input.next();
                            arrAppointments.add(new Onetime(strDate,strDescription));
                            break;
                        }
                        catch(Exception ArrayIndexOutofBoundsException){
                            System.out.println("Invalid Format of date, Please follow format : yyyy-mm-dd");
                        }
                    }
                }
            }
            else if(menuInput.equalsIgnoreCase("C"))
            {
                try{
                    System.out.println("Enter the year: ");
                    int nYear = input.nextInt();
                    System.out.println("Enter the month: ");
                    int nMonth = input.nextInt();
                    System.out.println("Enter the day: ");
                    int nDay = input.nextInt();

                    for(int i = 0; i < arrAppointments.size(); i++)
                    {
                        if(arrAppointments.get(i).occursOn(nYear,nMonth,nDay))
                        {
                            System.out.println(arrAppointments.get(i).getDescription());
                        }
                    }
                }
                catch(Exception InputMismatchException){
                    System.out.println("invalid input, follow format below: ");
                    System.out.println("                        year = ####  ");
                    System.out.println("                       month = ##    ");
                    System.out.println("                         day = ##  ");
                }
            }
        }
    }
}


