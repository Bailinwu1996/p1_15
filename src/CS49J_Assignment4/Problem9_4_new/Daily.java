package CS49J_Assignment4.Problem9_4_new;


/**
 * extends appointment class  for monthly appointment
 */

public class Daily extends Appointment {

    /**
     * constructs Daily object
     * @param strDate yyyy-mm-dd
     * @param strDescription description on appointment
     */
    public Daily(String strDate, String strDescription) {
        super(strDate, strDescription);
    }

    /**
     * check to see if occursOn is true/false
     * @param nYear  year
     * @param nMonth month
     * @param nDay day
     * @return true or false
     */
    public boolean occursOn(int nYear, int nMonth, int nDay)
    {
        return true;
    }
}
