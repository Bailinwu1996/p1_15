package CS49J_Assignment4.Problem9_13;

/**
 * JavaDoc
 * The java.awt.Rectangle class of the standard Java library does not supply a method to compute the area or perimeter of a rectangle.
 * creating a subclass BetterRectangle of the Rectangle class that has getPerimeter and getArea methods.
 */

import java.awt.Rectangle;

public class BetterRectangle extends Rectangle{

    /**
     * constructor of BetterRectangle using parent method by using super.
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public BetterRectangle(int x, int y, int width, int height){
        super.setLocation(x,y);
        super.setSize(width, height);
    }

    /**
     * getPerimeter is method that takes height and width and calculates the perimeter
     * @return perimeter of rectangle
     */
    public double getPerimeter(){
        return 2* (this.height * this.width);
    }

    /**
     * getArea is method that takes height and width and calculates the Area
     * @return area of rectangle
     */
    public double getArea(){
        return this.height * this.width;
    }

}
