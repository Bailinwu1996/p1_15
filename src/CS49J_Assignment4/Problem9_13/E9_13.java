package CS49J_Assignment4.Problem9_13;

import java.util.Scanner;

/**
 * Java doc
 * In E9_13 class, provide 3 test cases that tests the methods that you supplied printing expected and actual results.
 * The test cases are:
 * 1) One positive test case testing your method with valid inputs
 * 2) Two negative test cases, testing your methods for invalid inputs and
 *    printing proper error message indicating proper error handling.
 * @author Bailin Wu
 */

public class E9_13 {
    public static void main(String[] args) {

        int x_int = 0;
        int y_int =0;
        int width_int = 0;
        int length_int = 0;


        Scanner userInput = new Scanner(System.in);

        /**
         * prompting user to enter X value
         */
        System.out.println("Enter the Integer Value for X: ");
        String x_string = userInput.nextLine();
        if(isNumberic(x_string)){
            x_int = Integer.parseInt(x_string);
        }else if(x_string.contains(".")){
            System.out.println("Please do not enter any decimal ");
        }else{
            System.out.println("Please do not enter letter");
        }

        /**
         * Prompting user to enter Y value
         */
        System.out.println("Enter the Integer Value for Y: ");
        String y_string = userInput.nextLine();
        if(isNumberic(y_string)){
            y_int = Integer.parseInt(y_string); //turn string into integer
        }else if(y_string.contains(".")){
            System.out.println("Please do not enter any decimal ");
        }else{
            System.out.println("Please do not enter letter");
        }


        /**
         * Prompt user to enter width
         */
        System.out.println("Enter the Integer width Value of rectangle");
        String width_string = userInput.nextLine();
        if(isNumberic(width_string)){
            width_int = Integer.parseInt(width_string);
        }else if(width_string.contains(".")){
            System.out.println("Please do not enter any decimal ");
        }else{
            System.out.println("Please do not enter letter");
        }
        /**
         * Prompt user to enter length
         */
        System.out.println("Enter the Integer Length Value of rectangle");
        String length_string = userInput.nextLine();
        if(isNumberic(length_string)){
            length_int = Integer.parseInt(length_string);
        }else if(length_string.contains(".")){
            System.out.println("Please do not enter any decimal ");
        }else{
            System.out.println("Please do not enter letter");
        }

        /**
         * createa a perimeter Calculator that takes
         * x
         * y
         * width
         * length
         *
         */
        BetterRectangle Perim_Calc = new BetterRectangle(x_int, y_int, width_int, length_int);

        if( isNumberic(x_string) && isNumberic(y_string) && isNumberic(width_string) && isNumberic(length_string)){
            System.out.println("The rectangle is located in X : " + Perim_Calc.getX() + " Y : " + Perim_Calc.getY());
            System.out.println("The Area of the rectangle is : " + Perim_Calc.getArea());
            System.out.println("The Perimeter of rectangle is : " + Perim_Calc.getPerimeter());
        }
        else{
            System.out.println("wrong input, restart program");
        }

    }

    public static boolean isNumberic(String strNum){
        if(strNum == null){
            return false;
        }
        try{
            int d = Integer.parseInt(strNum);
        }catch(NumberFormatException nfe){
            return false;
        }
        return true;
    }
}
