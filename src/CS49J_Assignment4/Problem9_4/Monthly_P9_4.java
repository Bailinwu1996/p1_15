package CS49J_Assignment4.Problem9_4;

/**
 * sub class of Appointment class
 * takes in varible and distinguish it from other appointments
 */

public class Monthly_P9_4 extends Appointment_P9_4{

    /**
     * Constructor of Monthly class
     * @param year
     * @param month
     * @param day
     * @param description
     */
    public Monthly_P9_4(int year, int month, int day, String description){
        super(year, month, day, description);
    }

    /**
     * use to determine if there is matching dates with in the record
     * @param year
     * @param month
     * @param day
     * @return boolean result of appointment matches
     */

    public boolean occursOn(int year, int month, int day){
        if (year < getYear()){
            return false;
        }
        if (month < getMonth() && year == getYear()){
            return false;
        }
        return day == getDay();
    }

    /**
     * output type of appointment and appointment description
     * @return type of appointment and descrption of appointment
     */
    public String toString(){
        return "Monthly Appointment :" + super.getDescription();
    }

}