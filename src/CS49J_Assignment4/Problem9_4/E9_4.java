package CS49J_Assignment4.Problem9_4;
import java.util.Scanner;
import java.util.ArrayList;

/**
 * JavaDoc
 * This assignment models a tool with functionality of storing appointment with it's year/month/date
 * It can check the appointment by entering the year/month/date
 */

public class E9_4 {
    public static void main(String[] args){
        Scanner userInput = new Scanner(System.in);

        int day = 0;
        int year = 0;
        int month = 0;
        String description = "";
        String menuInput;
        String apntType;

        /**
         * construct a Appointment arraylist that contains methods and variables of project
         * @param year
         * @param month
         * @param day
         * @param description
         */
        ArrayList<Appointment_P9_4> apntArray = new ArrayList<Appointment_P9_4>();
        apntArray.add(new Daily_P9_4(2017, 1, 1, "work out"));
        apntArray.add(new Monthly_P9_4(2010, 1, 31, "pay rent"));


        /**
         * Appending block for user to add appointment according to their type.
         * using do while loop to take in user's input and allowing users to re-enter inputs
         */
        do{
            System.out.println("Select an option: A for add an appointment, C for checking, Q to quit: ");
            menuInput = userInput.nextLine();

            if(menuInput.equalsIgnoreCase("a")){
                do{
                    System.out.println("Enter the type (O-Onetime, D-Daily, or M-Monthly) B-Back to Main Menu:");
                    apntType = userInput.nextLine();
                    if(apntType.equalsIgnoreCase("o")){
                        try{
                            System.out.println("Enter the date (yyyy-mm-dd):");
                            String dateInput = userInput.nextLine();
                            String [] dateArray = dateInput.split("-",3);
                            year = Integer.parseInt(dateArray[0]);
                            month = Integer.parseInt(dateArray[1]);
                            day = Integer.parseInt(dateArray[2]);
                            System.out.println("Enter the description:");
                            description = userInput.nextLine();
                            apntArray.add(new Onetime_P9_4(year, month, day, description));
                        }catch(Exception ArrayIndexOutofBoundsException){
                            System.out.println("Invalid Format of date, Please follow format : yyyy-mm-dd");
                        }
                    }else if(apntType.equalsIgnoreCase("d")){
                        try{
                            System.out.println("Enter the date (yyyy-mm-dd):");
                            apntType = userInput.nextLine();
                            String [] dateArray = apntType.split("-",3);
                            year = Integer.parseInt(dateArray[0]);
                            month = Integer.parseInt(dateArray[1]);
                            day = Integer.parseInt(dateArray[2]);
                            System.out.println("Enter the description:");
                            description = userInput.nextLine();
                            apntArray.add(new Daily_P9_4(year, month, day, description));
                        }catch(Exception ArrayIndexOutofBoundsException){
                            System.out.println("Invalid Format of date, Please follow format : yyyy-mm-dd");
                        }
                    }
                    else if(apntType.equalsIgnoreCase("m")){
                        try{
                            System.out.println("Enter the date (yyyy-mm-dd):");
                            apntType = userInput.nextLine();
                            String [] dateArray = apntType.split("-",3);
                            year = Integer.parseInt(dateArray[0]);
                            month = Integer.parseInt(dateArray[1]);
                            day = Integer.parseInt(dateArray[2]);
                            System.out.println("Enter the description:");
                            description = userInput.nextLine();
                            apntArray.add(new Monthly_P9_4(year, month, day, description));
                        }catch(Exception ArrayIndexOutofBoundsException){
                            System.out.println("Invalid Format of date, Please follow format : yyyy-mm-dd");
                        }
                    }
                    else if(apntType.equalsIgnoreCase("b")){
                        System.out.println("returning to Main menu");

                    }
                    else if(!apntType.equalsIgnoreCase("m") && !apntType.equalsIgnoreCase("o") && !apntType.equalsIgnoreCase("m")){
                        System.out.println("Input value invalid, please enter O, M, or D");
                    }

                }
                while(!apntType.equalsIgnoreCase("b"));

            /**
             * Check Block for user to enter the year, month and date to check for appoiment for that day
             * **/
            }
            else if(menuInput.equalsIgnoreCase("c")) {

                // use try and catch here to avoid user entering the wrong input, allowing them to re-enter with out program breaking
                try{
                    System.out.print("Enter the year:");
                    year = userInput.nextInt();
                    System.out.print("Enter the Month:");
                    month = userInput.nextInt();
                    System.out.print("Enter the Day:");
                    day = userInput.nextInt();

                    for (Appointment_P9_4 appointment_p9_4 : apntArray) {
                        if (appointment_p9_4.occursOn(year, month, day)) {
                            System.out.println(appointment_p9_4);
                        }
                    }
                }catch(Exception InputMismatchException){
                    System.out.println("invalid input, follow format :");
                    System.out.println("                        year = ####  ");
                    System.out.println("                       month = ##    ");
                    System.out.println("                         day = ##  ");
                }
            }
            else if(!menuInput.equalsIgnoreCase("m") && !menuInput.equalsIgnoreCase("o") && !menuInput.equalsIgnoreCase("m") && !menuInput.equalsIgnoreCase("q") ){
                System.out.println("Input value invalid, please enter A, C, or Q");
            }
        }while(!menuInput.equalsIgnoreCase("q"));
        System.out.println("Program ended");

    }
}
