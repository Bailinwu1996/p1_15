package CS49J_Assignment4.Problem9_4;

/**
 * sub class of Appointment class
 * takes in varible and distinguish it from other appointments
 */
public class Onetime_P9_4 extends Appointment_P9_4 {
    /**
     * Constructor of Ontime class
     * @param year
     * @param month
     * @param day
     * @param description
     */
    public Onetime_P9_4(int year, int month, int day, String description) {
        super(year, month, day, description);
    }

    /**
     * output type of appointment and appointment description
     * @return
     */
    public String toString() {
        return "One Time Appointment :" + super.getDescription();
    }
}

