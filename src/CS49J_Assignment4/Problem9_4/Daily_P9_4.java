package CS49J_Assignment4.Problem9_4;

/**
 * sub class of Appointment class
 * takes in varible and distinguish it from other appointments
 */

public class Daily_P9_4 extends Appointment_P9_4{

    /**
     * Constructor of Daily class
     * @param year
     * @param month
     * @param day
     * @param description
     */
    public Daily_P9_4(int year, int month, int day, String description) {
        super(year, month, day, description);
    }


    /**
     * use to determine if there is matching dates with in the record
     * @param year
     * @param month
     * @param day
     * @return
     */

    public boolean occursOn(int year, int month, int day){
        return (year == super.getYear()) && (month == super.getMonth() && (day == super.getDay()));
    }

    /**
     * output type of appointment and appointment description
     * @return
     */

    public String toString(){
        return "Daily Appointment :" + super.getDescription();
    }
}
