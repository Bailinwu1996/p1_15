package CS49J_Assignment4.Problem9_4;

/**
 * JavaDoc a superclass moddling a Appointment recoding Note-book
 * Takes user's input
 * @author Bailin Wu
 */

public class Appointment_P9_4 {
    protected String description;
    protected int year;
    protected int month;
    protected int day;

    /**
     * Construct a appointment constructor so we can access protected variables
     * @param year
     * @param month
     * @param day
     * @param description
     */

    public Appointment_P9_4(int year, int month, int day, String description){
        this.year = year;
        this.month = month;
        this.day = day;
        this.description = description;
    }

    /**
     * use to access year varible
     * @return year
     */

    public int getYear(){
        return year;
    }

    /**
     * use to access month varible
     * @return month
     */

    public int getMonth(){
        return month;
    }

    /**
     * use to access day varible
     * @return month
     */

    public int getDay(){
        return day;
    }

    /**
     * use to access description varible
     * @return month
     */
    public String getDescription(){
        return description;
    }

    /**
     * use to determine if there is matching dates with in the record
     * @param year
     * @param month
     * @param day
     * @return boolean result of appointment matches
     */

    public boolean occursOn(int year, int month, int day){
        return (year == this.year) && (month == this.month) && (day == this.day);
    }

    /**
     * use to return what appointment is on that date
     * @return description
     */
    public String toString(){
        return description;
    }
}
