package CS49J_Assignment5;
import java.time.LocalDate;

abstract class Appointment {
    private String strDescription;
    private LocalDate date;

    /**
     * this constructor takes the strDate and strDescription and makes an appointment
     * @param strDate date: yyyy-mm-dd
     * @param strDescription what type of appointment it is
     */
    public Appointment(String strDate, String strDescription)
    {
        this.strDescription = strDescription;
        String[] arrayDate = strDate.split("-");
        date = LocalDate.of(Integer.parseInt(arrayDate[0]), Integer.parseInt(arrayDate[1]), Integer.parseInt(arrayDate[2]));
    }

    /**
     * gets the date of appointment
     * @return date
     */
    public LocalDate getDate()
    {
        return date;
    }

    /**
     * gets description of appointment
     * @return description
     */
    public String getDescription()
    {
        return strDescription;
    }


    /**
     * abstract method returns true if appointment occurs on date
     * @param nYear  year
     * @param nMonth month
     * @param nDay day
     * @return  if the date matches the meeting.
     */
    abstract boolean occursOn(int nYear, int nMonth, int nDay);

}


