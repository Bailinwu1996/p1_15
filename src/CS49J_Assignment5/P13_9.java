package CS49J_Assignment5;

import java.io.FileNotFoundException;

public class P13_9 {
    public static void main(String[] args){
        System.out.println(indexOf("Mississippi", "sip"));
    }

    public static int indexOf(String text, String str){

        //test to see if str is longer than text
        if (str.length() > text.length())
            return -1;
        //test to see if text and str have correct format
        if (text.length() <= 0 || str.length() <= 0)
            return -1;
        //testing if substring of text match with str
        if (text.substring(0,str.length()).equals(str))
            return 0;

        //recursion happen
        //continuously checking other substring of text and increment getInd
        int getInd = indexOf(text.substring(1,text.length()), str.substring(0,str.length()));
        if (getInd == -1)
            return -1;
        return 1 + getInd;
    }

}
