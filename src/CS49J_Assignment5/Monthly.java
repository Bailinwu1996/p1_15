package CS49J_Assignment5;


/**
 * extends appointment class  for monthly appointment
 */
public class Monthly extends Appointment {

    /**
     * constructs Monthly object
     * @param strDate yyyy-mm-dd
     * @param strDescription description on appointment
     */
    public Monthly(String strDate, String strDescription)
    {
        super(strDate, strDescription);

    }

    /**
     * check to see if occursOn is true/false
     * @param nYear  year
     * @param nMonth month
     * @param nDay day
     * @return true or false
     */
    public boolean occursOn(int nYear, int nMonth, int nDay)
    {
        return getDate().getDayOfMonth() == nDay;
    }

    public String toString(){
        return "Monthly Appointment : ";
    }
}
