package CS49J_Assignment5;
import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.ArrayList;
import java.io.PrintWriter;

/**
 * Bailin Wu
 * CS 49J
 * Improve from problem p_4 adding reading, appending, copying from txt file 
 */

public class P9_5 {
    public static void main(String[] args) throws IOException {
        ArrayList<Appointment> arrAppointments = new ArrayList<Appointment>(); // Create an ArrayList object
        Scanner input = new Scanner(System.in);
        String menuInput = "";

        while(!menuInput.equalsIgnoreCase("q"))
        {
            System.out.println("Select an option: A for add an appointment, C for checking, Q for quit:");
            menuInput = input.next();
            input.nextLine();
            if(menuInput.equalsIgnoreCase("a"))
            {
                String apntType = "";
                while(!apntType.equalsIgnoreCase("b")){
                    System.out.println("Enter the type (O - Onetime, D - Daily, M - Monthly or B - Back to Main menu");
                    apntType = input.next();
                    input.nextLine();
                    if(apntType.equalsIgnoreCase("o")){
                        try{
                            FileWriter fWriter = new FileWriter("appointments.txt", true);
                            PrintWriter outputFile = new PrintWriter(fWriter);
                            outputFile.print("OneTime/");
                            System.out.println("Enter the date (yyyy-mm-dd):");
                            outputFile.print(input.next() + "/");
                            System.out.println("Enter the description: ");
                            outputFile.println(input.next());
                            outputFile.close();
                            break;
                        }
                        catch(Exception ArrayIndexOutofBoundsException){
                            System.out.println("Invalid Format of date, Please follow format : yyyy-mm-dd");
                        }
                    }
                    else if(apntType.equalsIgnoreCase("d"))
                    {
                        try{
                            FileWriter fWriter = new FileWriter("appointments.txt", true);
                            PrintWriter outputFile = new PrintWriter(fWriter);
                            outputFile.print("Daily/");
                            System.out.println("Enter the date (yyyy-mm-dd):");
                            outputFile.print(input.next() + "/");
                            System.out.println("Enter the description: ");
                            outputFile.println(input.next());
                            outputFile.close();
                            break;
                        }
                        catch(Exception ArrayIndexOutofBoundsException){
                            System.out.println("Invalid Format of date, Please follow format : yyyy-mm-dd");
                        }
                    }
                    else if(apntType.equalsIgnoreCase("m"))
                    {
                        try{
                            FileWriter fWriter = new FileWriter("appointments.txt", true);
                            PrintWriter outputFile = new PrintWriter(fWriter);
                            outputFile.print("Monthly/");
                            System.out.println("Enter the date (yyyy-mm-dd):");
                            outputFile.print(input.next() + "/");
                            System.out.println("Enter the description: ");
                            outputFile.println(input.next());
                            outputFile.close();
                            break;
                        }
                        catch(Exception ArrayIndexOutofBoundsException){
                            System.out.println("Invalid Format of date, Please follow format : yyyy-mm-dd");
                        }
                    }
                }

            }
            else if(menuInput.equalsIgnoreCase("C"))
            {
                try{
                    System.out.println("Enter the year: ");
                    int nYear = input.nextInt();
                    System.out.println("Enter the month: ");
                    int nMonth = input.nextInt();
                    System.out.println("Enter the day: ");
                    int nDay = input.nextInt();

                    File myFile = new File("appointments.txt");
                    Scanner inputFile = new Scanner(myFile);
                    while(inputFile.hasNext()){
                        String[] arrOfStr = inputFile.nextLine().split("/",3);
                        String apntType = arrOfStr[0];
                        String strDate = arrOfStr[1];
                        String strDescription = arrOfStr[2];
                        if(apntType.equals("Monthly")){
                            arrAppointments.add(new Monthly(strDate,strDescription));
                        }
                        else if(apntType.equals("Daily")){
                            arrAppointments.add(new Daily(strDate,strDescription));
                        }
                        else if(apntType.equals("OneTime") ){
                            arrAppointments.add(new Onetime(strDate,strDescription));
                        }
                    }
                    inputFile.close();

                    for(int i = 0; i < arrAppointments.size(); i++)
                    {
                        if(arrAppointments.get(i).occursOn(nYear,nMonth,nDay))
                        {
                            System.out.println(arrAppointments.get(i).getDescription());
                        }
                    }

                }
                catch(Exception InputMismatchException){
                    System.out.println("invalid input, follow format below: ");
                    System.out.println("                        year = ####  ");
                    System.out.println("                       month = ##    ");
                    System.out.println("                         day = ##  ");
                }
            }
        }
        FileReader fr = null;
        FileWriter fw = null;
        try {
            fr = new FileReader("appointments.txt");
            fw = new FileWriter("savedApps.txt");
            int c = fr.read();
            while(c!=-1) {
                fw.write(c);
                c = fr.read();
            }
            System.out.println("All Appointment saved");
        } catch(IOException e) {
            e.printStackTrace();
        } finally {
            close(fr);
            close(fw);
        }
    }
    public static void close(Closeable stream) {
        try {
            if (stream != null) {
                stream.close();
            }
        } catch(IOException e) {
            System.out.println("file not found");
        }
    }

}


